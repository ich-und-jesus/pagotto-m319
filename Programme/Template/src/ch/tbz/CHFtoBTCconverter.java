package ch.tbz;

// Imports libraries
import static ch.tbz.lib.Input.inputDouble;
import static java.lang.System.*;   // System-IO Library for abbriviation purpose to static functions
import static java.lang.Math.*;     // Mathematic Library for abbriviation purpose to static functions
import java.util.*;                 // Random is part of this library

public class CHFtoBTCconverter {

    public static void main (String[] args) {

        // Title
        System.out.println("Umrechner von CHF zu BTC:\n");

        // Input
        double a = inputDouble("Geben Sie den CHF Betrag ein: ");

        // Calculation
        double b = a*0.000018;

        // System.Out function (Short version)
        System.out.println("Das Resultat ist: " + b);
    }
}
