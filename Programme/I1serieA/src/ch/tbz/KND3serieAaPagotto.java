package ch.tbz;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static ch.tbz.lib.Input.*;

public class KND3serieAaPagotto {
    public static void main(String[] args) {

        String[] WordArray = new String[5];
        ArrayList<Double> NumberArrayList = new ArrayList<Double>();
        int[] IntegerArray = new int[4];
        int i;

        System.out.println("Sortierungsprogramm D3 Serie A");

        System.out.println("Wörter Array");

        WordArray[0] = inputString("Geben Sie ein Wort ein");
        WordArray[1] = inputString("Geben Sie ein weiteres Wort ein");
        WordArray[2] = inputString("Geben Sie ein weiteres Wort ein");
        WordArray[3] = inputString("Geben Sie ein weiteres Wort ein");
        WordArray[4] = ".";

        System.out.println("Zahlen Array");

        NumberArrayList.add(inputDouble("Geben Sie eine Zahl ein"));
        NumberArrayList.add(inputDouble("Geben Sie eine weitere Zahl ein"));
        NumberArrayList.add(inputDouble("Geben Sie eine weitere Zahl ein"));
        NumberArrayList.add(inputDouble("Geben Sie eine weitere Zahl ein"));

        System.out.println("String Array");

        IntegerArray[0] = inputInt("Geben Sie eine Zahl ein");
        IntegerArray[1] = inputInt("Geben Sie eine weitere Zahl ein");
        IntegerArray[2] = inputInt("Geben Sie eine weitere Zahl ein");
        IntegerArray[3] = inputInt("Geben Sie eine weitere Zahl ein");

        Arrays.sort(WordArray);

        Collections.sort(NumberArrayList);

        Arrays.sort(IntegerArray);

        String StringFromArray = Arrays.toString(IntegerArray);

        for(i=0; i<WordArray.length; i++) {
            System.out.println(WordArray[i]);
        }

        for (i=0; i< NumberArrayList.size(); i++ ) {
            System.out.println(NumberArrayList.get(i));
        }
        System.out.println(StringFromArray);
    }

}
