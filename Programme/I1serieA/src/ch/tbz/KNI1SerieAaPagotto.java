// Defines package
package ch.tbz;

// Imports libraries
import java.math.*;                 // Mathematic Library for abbriviation purpose to static functions


// Imports Input functions
import static ch.tbz.lib.Input.*;       // All the functions can be used now!

//Every program must be placed in a class ...
public class KNI1SerieAaPagotto {

    // Our main function which runs the program
    public static void main(String[] args ) {


        //Title
        System.out.println("I1 Serie A Currency Converter for Product Orders:\n\n");
        String g;

        do {
            // Input of Values
            String a = inputString("Enter Original Currency: ");
            double b = inputDouble("Enter Original Price: ");
            double c = inputDouble("Enter Transport and custom costs: ");
            String d = inputString("Enter target Currency: ");

            // Currency Selection

            double f = 0;

            if (a.equalsIgnoreCase("USD")) {
                switch (d) {
                    case ("EUR"):
                        f = (b+c) * 0.91401033;
                        break;
                    case ("CHF"):
                        f = (b+c) * 0.87772983;
                        break;
                    default:
                        System.out.println("Please Enter a Currency in the 3-digit format");
                        break;
                }
            } else if (a.equalsIgnoreCase("EUR")) {
                switch (d) {
                    case ("USD"):
                        f = (b+c) * 1.0940795;
                        break;
                    case ("CHF"):
                        f = (b+c) * 0.96030625 ;
                        break;
                    default:
                        System.out.println("Please Enter a Currency in the 3-digit format");
                        break;
                }
            } else if (a.equalsIgnoreCase("CHF")) {
                switch (d) {
                    case ("USD"):
                        f = (b+c) * 1.1393027;
                        break;
                    case ("EUR"):
                        f = (b+c) * 1.0413345;
                        break;
                    default:
                        System.out.println("Please Enter a Currency in the 3-digit format");
                        break;
                }
            } else {
                System.out.println("Please Enter a Currency in the 3-digit format");
            }


            BigDecimal bd = new BigDecimal(f).setScale(2, RoundingMode.HALF_UP);
            double r = bd.doubleValue();


            // Result
            System.out.println("Result is:" + r);

            //Restart or quit

            g = inputString("Do you want to calculate again? [Y]/[N]");

        }
        while (g.equalsIgnoreCase("Y")) ;



    }
}