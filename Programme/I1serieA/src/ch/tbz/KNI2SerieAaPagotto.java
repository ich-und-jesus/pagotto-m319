package ch.tbz;

// Import of Input function
import static ch.tbz.lib.Input.*;

//Program
public class KNI2SerieAaPagotto {

    public static void main(String[] args ) {
        System.out.println("I2 Serie A, vergleich von Zahlenwerten bis diese gleich sind");

        boolean isEqual;

        // Input of Numbers

        do {
            int a = inputInt("Geben Sie die erste Ganze Zahl ein");
            int b = inputInt("Geben Sie die zweite Ganze Zahl ein");
            int c = inputInt("Geben Sie die dritte Ganze Zahl ein");


        isEqual = a == b & b == c;

            // Lowest and Highest number

            int k = 0;
            int g = 0;

            if (a <= (b|c)) {
                k = a;
            } else if (b <= (a|c)) {
                k = b;
            } else {
                k = c;
            }
            if (a >= (b|c)) {
              g = a;
            } else if (b >= (a|c)) {
                g = b;
            } else {
                g = c;
            }


            // Output
            System.out.println("Kleinster Zahlenwert:"+ k);
            System.out.println("Grösster Zahlenwert:"+ g);
            if (isEqual) {
                System.out.println("Zahlenwerte identisch");
            } else {
                System.out.println("Zahlenwerte nicht identisch");
            }
        } while (!isEqual);
    }
}
